<?php

/*
 * - Créer une variable nombreAtrouver et lui affecter une valeur aléatoire
 * - afficher "choisir un nombre"
 * - stocker la réponse dans la variable réponse
 * - Si réponse < nombreAtrouver
 *  afficher "c'est plus"
 * - sinon si réponse > nombreAtrouver
 *  afficher "c'est moins"
 * - sinon
 *  affiché c'est gagné
 *  jouer de la musique
 *
 */

$nombreAtrouver =  mt_rand (0 , 100);
var_dump($nombreAtrouver);
// echo $nombreAtrouver . "nombre à trouver";

$reponse = readline("Choisir un nombre : ");
var_dump($reponse);

while ($nombreAtrouver != $reponse){
    if ($reponse < $nombreAtrouver){
        echo "C'est plus !";
    }
    else if ($reponse > $nombreAtrouver){
        echo "C'est moins !";
    }
    $reponse = readline("Choisir un nombre : ");
}
echo "Bravo vous avez trouvé !";